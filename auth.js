const jwt = require("jsonwebtoken");

// User defined string data that will be used to create our JSON web tokens
// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword
//What is the secret key?
//The secret key is combined with the header and the payload to create a unique hash. You are only able to verify this hash if you have the secret key.
const secret = "CourseBookingAPI";

// [ SECTION ] JSON Web Tokens
	// JWT is a way of securely passing information from the server to the frontend or to other parts of server
	// Information is kept secure through the use of the secret code
	// Only the system that knows the secret code that can decode the encrypted information

module.exports.createAccessToken = (user) => {
	
	// The data will be received from the registration form
	// When the user logs in, a token will be created with user's information
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	};

	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {});
};

module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;
	//console.log(token)

	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);
		//console.log(token);

		return jwt.verify(token, secret, (err, data) => {

			if(err){
				return res.send({auth : "failed"});
			} else {
				next()
			}
		})
	} else {
		return res.send({auth : "failed"});
	}
}

module.exports.decode = (token) => {

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {

			if(err){
				return null;
			} else {

				return jwt.decode(token, {complete:true}).payload;
			};
		})
	} else {
		return null;
	}
}
